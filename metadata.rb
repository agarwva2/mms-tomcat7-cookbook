name             "mms-tomcat7"
maintainer       "Novartis"
maintainer_email "devops@novartis.com"
license          "All rights reserved"
description      "Installs/Configures environment for cims"
version          "0.1.1"

depends "ci"
depends "mw-tomcat"

