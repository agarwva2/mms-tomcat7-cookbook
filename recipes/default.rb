node.default['tomcat']['base_version'] = 7

include_recipe "mw-tomcat"

case deploy_env
when nil, ''
  env='dev'
when 'release'
  env='prod'
when 'test'
  env='test'
when 'prod'
  env='prod'
when 'ci'
  env='dev'
else
  env='dev'
end

